#include <stdio.h>

#define N	3

#ifndef FALSE
#define FALSE	0
#endif

#ifndef TRUE
#define TRUE	(!FALSE)
#endif

#define dr(r, i)	(r ? (N-i-1) : i)


void fill_in(int cube[N][N][N])
{
    int x, y, z;
    int i = 1;

    for (z=0; z<N; z++)
        for (y=0; y<N; y++)
            for (x=0; x<N; x++)
                cube[z][y][x] = i++;
}

int main()
{
    int cube[N][N][N];
    int d1, d2, d3;
    int *x, *y, *z;
    int rx, ry, rz;

    fill_in(cube);

    /* Dimensions order */
    x = &d1;
    y = &d2;
    z = &d3;

    /* Reverse dimension directions */
    rx = FALSE;
    ry = FALSE;
    rz = FALSE;

    for (d3=0; d3<N; d3++)
        for (d2=0; d2<N; d2++)
            for (d1=0; d1<N; d1++)
                printf("%d ", cube[dr(rz, *z)][dr(ry, *y)][dr(rx, *x)]); 
    printf("\n");

    return 0;
}
